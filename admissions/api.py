# admissions/api.py 
from tastypie.resources import ModelResource 
from tastypie.resources import Resource
from tastypie.resources import ALL, ALL_WITH_RELATIONS
from tastypie import fields 
from django.db.models import Count
from admissions.models import Student
import datetime

class StudentResource(ModelResource): 
	class Meta: 
		queryset = Student.objects.all()
		resource_name = 'student'
		max_limit = None
		filtering = {
			'id': ALL,
			'first_name': ALL,
			'age': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
			'gender': ALL,
			'branch': ALL,
			'stream': ALL,
			'board': ALL,
			'city': ALL,
			'state': ALL,
			'score': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
			'created_at': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
		}
	def dehydrate(self, bundle):
		temp_date = bundle.data['created_at'].strftime('%Y-%m-%d')
		bundle.data['date'] = temp_date
		bundle.data['city'] = bundle.data['city'].upper()
		bundle.data['board'] = bundle.data['board'].upper()
		bundle.data['branch'] = bundle.data['branch'].upper()
		bundle.data['state'] = bundle.data['state'].upper()
		bundle.data['stream'] = bundle.data['stream'].upper()
		return bundle