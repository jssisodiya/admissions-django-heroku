import datetime
from django.db import models
from django.utils import timezone

class Student(models.Model):
	first_name = models.CharField(max_length=250)
	last_name = models.CharField(max_length=250)
	age = models.IntegerField()
	gender = models.CharField(max_length=1)
	address = models.CharField(max_length=250)
	email_id = models.CharField(max_length=250)
	branch = models.CharField(max_length=250)
	school_name = models.CharField(max_length=250)
	stream = models.CharField(max_length=250)
	board = models.CharField(max_length=250)
	city = models.CharField(max_length=250)
	state = models.CharField(max_length=250)
	score = models.CharField(max_length=250)
	created_at = models.DateTimeField('date published')