# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=250)),
                ('last_name', models.CharField(max_length=250)),
                ('age', models.IntegerField()),
                ('gender', models.CharField(max_length=1)),
                ('address', models.CharField(max_length=250)),
                ('email_id', models.CharField(max_length=250)),
                ('branch', models.CharField(max_length=250)),
                ('school_name', models.CharField(max_length=250)),
                ('stream', models.CharField(max_length=250)),
                ('board', models.CharField(max_length=250)),
                ('city', models.CharField(max_length=250)),
                ('state', models.CharField(max_length=250)),
                ('score', models.CharField(max_length=250)),
                ('created_at', models.DateTimeField(verbose_name=b'date published')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
