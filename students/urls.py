from django.conf.urls import patterns, include, url
from django.contrib import admin
from admissions.api import StudentResource

student_resource = StudentResource() 

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'students.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(student_resource.urls)),
)
